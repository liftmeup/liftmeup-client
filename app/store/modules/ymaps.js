import * as types from '../mutation-types';
import ymaps from 'ymaps';

// initial state
const state = {
  isApiAvailable: false,
  activeLayout: 'default',
  layouts: {
    default: '',
    routeDetails: {
      id: '',
    },
  },
};

// getters
const getters = {
  isApiAvailable: state => state.isApiAvailable,
  layoutOptions: ({ layouts }, { activeLayout }) => state.layouts[activeLayout],
  activeLayout: state => state.activeLayout
};

// actions
const actions = {
  async loadYmapApi({ dispatch, commit, state }) {
    commit(types.YMAP_API_LOADING);

    try {
      const mapApi = await ymaps.loadApi();

      commit(types.YMAP_API_LOADED);

      return mapApi;
    } catch (error) {
      commit(types.YMAP_API_FAILED);
      throw new Error(`Could not load yandex map api: ${error}`);
    }
  },
  async initMap({ dispatch, commit, state }, { elementId = 'map', props = {
    center: [54.9885, 73.3242],
    zoom: 10,
    controls: []
  }}) {
    await dispatch('loadYmapApi');
    ymaps.createMap(elementId, props);
    dispatch('applyLayout');
  },
  setLayout({ dispatch, commit }, layout) {
    commit(types.YMAP_SET_LAYOUT, layout);
  },
  applyLayout({ state: { layouts, activeLayout: type }, dispatch }, layout) {
    dispatch(`${type}Layout`, layouts[type]);
  },

  // layouts
  defaultLayout() {
    ymaps.clearMap();
  },
  async routeDetailsLayout({ dispatch }, { id }) {
    const route = await dispatch('routes/getRouteById', id, { root: true });
    const routeObject = ymaps.createRoute(route);
    ymaps.replaceCollection(routeObject);
  },
};

// mutations
const mutations = {
  [types.YMAP_API_LOADED](state) {
    state.isApiAvailable = true;
  },

  [types.YMAP_API_FAILED](state) {
    state.isApiAvailable = false;
  },

  [types.YMAP_API_LOADING](state) {
    state.isApiAvailable = false;
  },

  [types.YMAP_SET_LAYOUT](state, { type, params }) {
    state.activeLayout = type;
    state.layouts[type] = params;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
