import * as types from '../../../mutation-types';
import ymaps from 'ymaps';

// initial state
const state = {
  city: 'Омск',
  newRoute: {
    pointA: '',
    pointB: '',
    transitPoints: []
  }
};

// getters
const getters = {
  newRoutePoints: state => state.newRoute
};

// actions
const actions = {
  updateFormModel({ commit, state }, route) {
    commit(types.ROUTE_CONTROL_UPDATE, route);
  },
  addRouteToMap({ commit, state, dispatch }, { pointA, pointB }) {
    const route = ymaps.createRoute([pointA, pointB]);
    ymaps.addToMap(route);
    return route;
  }
};

// mutations
const mutations = {
  [types.ROUTE_CONTROL_UPDATE](state, { pointA, pointB, transitPoint }) {
    const transitPoints = state.newRoute.transitPoints;

    state.newRoute = { ...state.newRoute, pointA, pointB };
    state.newRoute.transitPoints = [...transitPoints, transitPoint];
  },
  [types.ROUTE_CONTROL_SET_ROUTE_OBJECT](state, route) {
    state.routeObject = route;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
