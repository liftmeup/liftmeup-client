import Vue from 'vue';
import App from 'containers/App';

Vue.directive('modifiers', {
  bind(el, binding, vnode) {
    const component = vnode.componentInstance;
    const componentName = vnode.componentOptions.tag;
    const arg = binding.arg ? `__${binding.arg}` : '';

    const classTemplate = `${componentName}${arg}--`;
    const keys = Object.keys(binding.modifiers);

    keys.forEach(key => component.$set(component.classes, `${classTemplate}${key}`, true));
  },
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App />',
  components: { App },
});
