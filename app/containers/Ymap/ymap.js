import { mapGetters, mapActions } from 'vuex';

export default {
  name: 'ymap',
  props: {
    elementId: {
      type: String,
      default: 'map',
    }
  },
  mounted: function() {
    const { meta: { layout }, params } = this.$route;
    const type = layout || 'default';

    this.setLayout({ type, params });
    this.initMap(this.elementId);
  },
  computed: {
    ...mapGetters('ymaps', [
      'isApiAvailable',
    ]),
  },
  methods: {
    ...mapActions('ymaps', [
      'initMap',
      'setLayout',
    ]),
  },
};
