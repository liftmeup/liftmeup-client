import store from 'store';
import Ymap from 'containers/Ymap';
import NotificationContainer from 'containers/NotificationContainer';
import Vue from 'vue';
import { mapActions } from 'vuex';
import VueRouter from 'vue-router';
import routes from '../../routes';

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  const { dispatch, getters } = store;

  if (getters['ymaps/isApiAvailable']) {
    const { meta, params } = to;
    const type = meta.layout || 'default';

    dispatch('ymaps/setLayout', { type, params });
    dispatch('ymaps/applyLayout');
  }

  next();
});

Vue.use(VueRouter);

export default {
  name: 'app',
  components: {
    Ymap,
    NotificationContainer,
  },
  data() {
    return { };
  },
  methods: {
    ...mapActions('routes', [
      'loadRoutes',
    ]),
  },
  created() { this.loadRoutes() },
  store,
  router,
};
