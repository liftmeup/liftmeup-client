import { load } from '../base';

let page;
let addRouteSection;

export default {
  before: (client) => {
    load(client);
    page = client.page.liftmeup();
    addRouteSection = page.section.addRoute;
  },
  after: (client) => {
    client.end();
  },
  'On load': function onLoad(client) {
    page.expect.section('@addRoute').to.be.present;
    page.expect.section('@addRoute').to.has.attribute('class').which.contain('button-menu--collapsed');
  },
  'On click': function onClick(client) {
    addRouteSection.click('@button');

    page.expect.section('@addRoute').to.has.attribute('class').which.does.not.contain('button-menu--collapsed');

    addRouteSection.expect.section('@content').to.be.visible;

    const contentSection = addRouteSection.section.content;

    contentSection.expect.element('@inputPointA').to.be.present;
    contentSection.expect.element('@inputPointB').to.be.present;
    contentSection.expect.element('@button').to.be.present;
  },
  'On second click': function onDoubleClick() {
    addRouteSection.click('@button');
    page.expect.section('@addRoute').to.has.attribute('class').which.contain('button-menu--collapsed');
    addRouteSection.expect.section('@content').to.not.be.visible.before(2000);
  }
};
