const elements = {
};

const sections = {
  addRoute: {
    selector: '.route-form',
    elements: {
      button: {
        selector: './button[1]',
        locateStrategy: 'xpath'
      }
    },
    sections: {
      content: {
        selector: './div[1]/div[1]/div[1]',
        locateStrategy: 'xpath',
        elements: {
          inputPointA: {
            selector: './div[1]/input[1]',
            locateStrategy: 'xpath'
          },
          inputPointB: {
            selector: './div[2]/input[1]',
            locateStrategy: 'xpath'
          },
          button: {
            selector: './button[1]',
            locateStrategy: 'xpath'
          }
        }
      }
    }
  }
};

export {
  elements,
  sections
};
