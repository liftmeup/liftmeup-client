// https://github.com/shelljs/shelljs
import checkVersions from './check-versions';
import 'shelljs/global';
import path from 'path';
import config from '../config/env';
import ora from 'ora';
import webpack from 'webpack';
import webpackConfig from '../config/webpack.prod.conf';

checkVersions();
env.NODE_ENV = 'production';

console.log(
  '  Tip:\n' +
  '  Built files are meant to be served over an HTTP server.\n' +
  '  Opening index.html over file:// won\'t work.\n'
);

const spinner = ora('building for production...');
spinner.start();

const assetsPath = path.join(config.build.assetsRoot, config.build.assetsSubDirectory);

rm('-rf', assetsPath);
mkdir('-p', assetsPath);
cp('-R', 'static/*', assetsPath);

webpack(webpackConfig, (err, stats) => {
  spinner.stop();

  if (err) {
    throw err;
  }

  process.stdout.write(stats.toString({
    colors: true,
    modules: false,
    children: false,
    chunks: false,
    chunkModules: false
  }) + '\n');
});
